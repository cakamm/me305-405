'''
@file BLEmain.py
@brief User interface task which receives a frequency and blinks an LED at the given frequency

'''

import pyb
import utime
from pyb import UART
myuart = UART(2)
class BLEDriver:
    def __init__(self):
        '''Method initializing BLE Driver and specifying pin callout'''
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)     
            
    def On(self):
        '''A method which turns the light on'''
        self.pinA5.high()
        
    def Off(self):
        '''A method which turns the light off'''
        self.pinA5.low()
    
    def read(self):
        '''A method which reads integer inputs and assigns them a variable'''
        self.val = int(myuart.readline())
       
    
    def write(self, arg):
        '''A method which writes the frequency at which the light is blinking'''
        myuart.write('LED blinking at '+ str(arg) + ' Hz')
        
    def check(self):
        if myuart.any():
            return 'Frequency received'
        
class TaskUser:
    '''
    User interface task.
    
    An object of this class interacts with serial port UART2 waiting for 
    frequency values. When a value is entered, this task will blink an LED on 
    and off at the specified frequency.
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for frequency
    S1_WAIT_FOR_CHAR    = 1
    
    ## Light on state
    S2_ON               = 2
    
    ## Light off state
    S3_OFF              = 3

    def __init__(self, dbg=True):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        ## Class object for BLE Driver
        self.BLE = BLEDriver()
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(0.001*(1e6))
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.BLE.write('Please enter a frequency between 1 and 10 Hz.')
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                
                self.printTrace()
                # Run State 1 Code
                if self.BLE.check():
                    self.BLE.read()
                    self.transitionTo(self.S2_ON)
                    self.BLE.write(self.BLE.val)
                    self.interval = 1/(int(self.BLE.val))
                    
            
            elif(self.state == self.S2_ON):
                self.BLE.ON()
                self.BlE.val = None
                self.transitionTo(self.S3_OFF)
                
            elif (self.state == self.S3_OFF):
                self.BLE.OFF()
                if self.BLE.check():
                    self.BLE.read()
                    self.transitionTo(self.S2_LIGHT)
                    self.BLE.write(self.BLE.val)
                    self.interval = 1/(int(self.BLE.val))
                else:
                    pass
                self.transitionTo(self.S2_ON)
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
            
task = TaskUser(dbg=False)
while True:
    task.run()    