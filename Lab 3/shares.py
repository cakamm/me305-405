'''
@file shares.py
@brief A container for all the inter-task variables

'''

## The command character sent from the user interface task to the encoder task
cmd     = None

## The response from the encoder task after encoding
resp    = None