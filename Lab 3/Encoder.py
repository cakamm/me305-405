'''
@file Encoder.py
@brief A file which creates an Encoder class and a finite state machine to output and set encoder position.
'''
import shares 
import utime
import pyb
class Encoder:
    
    def __init__(self, timer, Pin1, Pin2):
        self.timer = timer
        self.Pin1 = Pin1
        self.Pin2 = Pin2
         
        tim = timer
        tim.init(prescaler=0, period=0xFFFF)
        tim.channel(1, pin=Pin1, mode=pyb.Timer.ENC_AB)
        tim.channel(2, pin=Pin2, mode=pyb.Timer.ENC_AB)
        
    def update(self):
        '''
        @brief This function updates the encoder position each time it is called.
        '''
        self.newposition = self.timer.counter()
        self.delta = self.newposition - self.oldposition
        if abs(self.delta) > (0.5*0xFFFF):
            if self.delta > 0:
                self.realdelta = 0xFFFF - self.delta
            elif self.delta < 0:
                self.realdelta = 0xFFFF + self.delta
            else:
                pass
        #elif self.delta == 0:
          #  self.realdelta = 0
        else:
            self.realdelta = self.delta
        self.realposition = self.oldposition + self.realdelta
        
        
    def get_position(self):
        '''
        @brief This function returns the current position of the motor.
        '''
        return self.realposition
        
    def set_position(self, param):
        '''
        @brief This function returns the current position of the motor.
        '''
        self.timer.counter(param)
        
    def get_delta(self):
        '''
        @brief This function returns the difference between the current motor 
        position and the previous motor position.
        '''
        return self.realdelta

        
class FSM_Encoder:
    '''
    @brief A finite state machine which receives commands and sets and records encoder position
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT                  = 0
    
    ## Constant defining State 1
    S1_UPDATE                = 1
    
    ## Constant defining State 2
    S2_COMMAND               = 2
    
    
    
    def __init__(self, interval, Enco):
        '''
        @brief      Creates a FSM_Encoder object.
        '''
        
        
        ## Class object for Encoder.
        self.Enco = Encoder(pyb.Timer(3), pyb.Pin.cpu.A6, pyb.Pin.cpu.A7)
        
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since Jan 1. 1970
        
        ## The interval of time, in microseconds, between runs of the task
        self.interval = interval
        
         ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                self.Enco.oldposition = 0
                #print('state 0')
                self.transitionTo(self.S1_UPDATE)
                
            elif (self.state == self.S1_UPDATE):
                self.Enco.update()
                #print('state 1')
                #print(str(self.Enco.realposition))
                self.Enco.oldposition = self.Enco.realposition
                if shares.cmd:
                    self.transitionTo(self.S2_COMMAND)
                else:
                    self.transitionTo(self.S1_UPDATE)
                
            elif (self.state == self.S2_COMMAND):
                #print('state 2')
                if shares.cmd:
                    #print(str(shares.cmd))
                    if shares.cmd == 122:
                        self.Enco.set_position(0)
                        self.Enco.realposition = 0
                        shares.resp = 'encoder position has been zeroed'
                    elif shares.cmd == 112:
                        shares.resp = (self.Enco.get_position())/3.89
                        #print('position is' + str(shares.resp))
                    elif shares.cmd == 100:
                        shares.resp = (self.Enco.get_delta())/3.89
                    else:
                        pass
                    shares.cmd = None
                else:
                    pass
                self.transitionTo(self.S1_UPDATE)
                
            
            self.runs += 1   
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        