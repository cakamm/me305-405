'''
@file main.py
@brief Main program runs all the tasks round-robin
'''
import pyb
from User import TaskUser
from Encoder import FSM_Encoder, Encoder 

## Encoder class object
Encoder = Encoder(pyb.Timer(3), pyb.Pin.cpu.A6, pyb.Pin.cpu.A7)

## User interface task, works with serial port
task0 = TaskUser(0, 1_000, dbg=False)

## Cipher task encodes characters sent from user task by flipping the case of the character if it is a letter
task1 = FSM_Encoder(1_000, Encoder)

## The task list contains the tasks to be run "round-robin" style
taskList = [task0,
            task1]

while True:
    for task in taskList:
        task.run()
        