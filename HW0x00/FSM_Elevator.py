'''
@file FSM_Elevator.py

This file serves as an implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary
elevator which moves between two floors.

The user has a button for each floor to send elevator to first or second floor.

There is also a limit switch (proximity sensors) at both floors the elevator can move between.
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                  = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN           = 1
    
    ## Constant defining State 2
    S2_MOVING_UP             = 2
    
    ## Constant defining State 3
    S3_STOPPED_ON_FLOOR_1    = 3
    
    ## Constant defining State 4
    S4_STOPPED_ON_FLOOR_2    = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING            = 5
    
    def __init__(self, interval, Motor, button_1, button_2, first, second):
        '''
        @brief      Creates a TaskElevator object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the button_1 object
        self.button_1 = button_1
        
        ## A class attribute "copy" of the button_2 object
        self.button_2 = button_2
        
        ## The button object used for the proximity sensors at first floor landing
        self.first = first
        
        ## The button object used for the proximity sensors at second floor landing
        self.second = second
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Reverse()
            
            elif(self.state == self.S3_STOPPED_ON_FLOOR_1):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                
                if self.runs*self.interval > 2:
                    self.transitionTo(self.S5_DO_NOTHING)
                
                elif self.button_2.getButtonState():
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Forward()
                    #self.button_2 == False
            
            elif(self.state == self.S4_STOPPED_ON_FLOOR_2):
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.button_1.getButtonState():
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Reverse()
            
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                if self.first.getButtonState():
                    self.transitionTo(self.S3_STOPPED_ON_FLOOR_1)
                    self.Motor.Stop()
            
            elif(self.state == self.S2_MOVING_UP):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                if self.second.getButtonState():
                    self.transitionTo(self.S4_STOPPED_ON_FLOOR_2)
                    self.Motor.Stop()
                    
            elif(self.state == self.S5_DO_NOTHING):
                print(str(self.runs) + ' State 5 {:0.2f}'.format(self.curr_time - self.start_time))
                
            
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Forward(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving elevator up')
    
    def Reverse(self):
        '''
        @brief Moves the motor in reverse
        '''
        print('Motor moving elevator down')
    
    def Stop(self):
        '''
        @brief Stops motor movement
        '''
        print('Motor Stopped')

