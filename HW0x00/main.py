'''
@file main.py
'''

from FSM_Elevator import Button, MotorDriver, TaskElevator
        
## Motor Object for Task 1
Motor1 = MotorDriver()

## Button Object for "button_1" for Task 1
button_11 = Button('PB6')

## Button Object for "button_2" for Task 1
button_21 = Button('PB7')

## Button Object for "first" for Task 1
first1 = Button('PB8')

## Button Object for "second" for Task 1
second1 = Button('PB9')

## Motor Object for Task 2
Motor2 = MotorDriver()

## Button Object for "button_1" Task 2
button_12 = Button('PC6')

## Button Object for "button_2" Task 2
button_22 = Button('PC7')

## Button Object for "first" Task 2
first2 = Button('PC8')

## Button Object for "second" Task 2
second2 = Button('PC9')

## Task object
task1 = TaskElevator(0.1, Motor1, button_11, button_21, first1, second1) # Will also run constructor

## Second Task object
task2 = TaskElevator(0.1, Motor2, button_12, button_22, first2, second2)

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()

