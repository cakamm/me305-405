''' 
@file fib.py
@brief A file which produces a requested Fibonacci number.
@details This file first prompts the user for the index of their desired 
Fibonacci number and produces the requested number. It then asks if the user
is finished. If the user responds "yes", the program will end; however if
the user enters any other response, the program will prompt the user for
another index.
@author Carissa Kamm
 '''


def fib (idx):
    ''' This method calculates a Fibonacci number corresponding to
    a specified index.
    @param idx An integer specifying the index of the desired
              Fibonacci number.'''
    
    if idx == 0:
        print(0)
    elif idx == 1:
        print(1)
    elif idx < 0:
        print('Sorry, cannot compute Fibonacci Sequence for non-positive integers or non-integers.')
    elif idx > 1:
        fiblist = [0,1]
        n = 2
        while n <= idx:
            fiblist.append(fiblist[n-1]+fiblist[n-2])
            n = n + 1
        print('The Fibonacci number you requested is:')
        print(fiblist[idx])   
   
        
   
        
        
if __name__ == '__main__':
   while True:
       idx = input('Please enter an index for a desired Fibonacci number:')
       try:
           val = int(idx)
           fib(val)
       except ValueError:
           print('This is not a positive integer. Please enter a positive integer.')
       answer = input('Do you want to exit out of the program?')
       if answer == 'yes':
          import sys
          sys.exit()