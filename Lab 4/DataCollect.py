'''
@file DataCollect.py
'''
 
import utime
import pyb
from pyb import UART


class EncoderDriver:
    '''
    @brief A class to set up the Nucleo to read Encoder position 
    and define the functions to interpret, set, and return encoder position.
    '''
    
    def __init__(self, timer, Pin1, Pin2):
        self.timer = timer
        self.Pin1 = Pin1
        self.Pin2 = Pin2
         
        tim = timer
        tim.init(prescaler=0, period=0xFFFF)
        tim.channel(1, pin=Pin1, mode=pyb.Timer.ENC_AB)
        tim.channel(2, pin=Pin2, mode=pyb.Timer.ENC_AB)
        
    def update(self):
        '''
        @brief This function updates the encoder position each time it is called.
        '''

        self.newposition = self.timer.counter()
        self.delta = self.newposition - self.oldposition
        if (abs(self.delta) > (0.5*(0xFFFF + 1))):
            if (self.delta > 0):
                self.realdelta =  - self.delta - (0xFFFF + 1)
            elif (self.delta < 0):
                self.realdelta = self.delta + (0xFFFF + 1) 
        else:
            self.realdelta = self.delta
        self.realposition = self.oldposition + self.realdelta
        self.oldposition = self.realposition
        
    def get_position(self):
        '''
        @brief This function returns the current position of the motor.
        '''
        return self.realposition
        
    def set_position(self, param):
        '''
        @brief This function returns the current position of the motor.
        '''
        self.tim.counter(param)
        
    def get_delta(self):
        '''
        @brief This function returns the difference between the current motor 
        position and the previous motor position.
        '''
        return self.realdelta


        
class Data_Collect:
    '''
    @brief A finite state machine which interacts with the front end user interface
    to receive commands and send data.
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT                  = 0
    
    ## Constant defining State 1
    S1_COLLECT               = 1
    
    ## Constant defining State 2
    S2_STOP                  = 2
    
    
    
    def __init__(self, Enco):
        '''
        @brief      Creates a Data_Collect object.
        '''
        ## Class object for Encoder.
        self.Enco = EncoderDriver(pyb.Timer(3), pyb.Pin.cpu.A6, pyb.Pin.cpu.A7)
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since Jan 1. 1970
        
        ## The interval of time, in seconds, between runs of the task
        self.interval = int((1/5)*1e6)
        
         ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Initial encoder position
        self.Enco.oldposition = 0
        
        ## List for time
        self.t = [0]
        
        ## List for position
        self.x = [0]
        
        ## UART bus used for Nucleo
        self.myuart = UART(2)
        

    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        self.read_time = utime.ticks_diff(self.curr_time, self.start_time)  #current timestamp starting from 0
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                if self.myuart.any() !=0:
                    self.val = self.myuart.readchar()
                    if self.val == 103:
                        self.transitionTo(self.S1_COLLECT)
                        self.val = None
                    
            elif (self.state == self.S1_COLLECT):
                self.Enco.update()
                self.t.append(self.read_time)
                self.x.append((self.Enco.realposition)/3.89)
                if self.val == 115:
                    self.transitionTo(self.S2_STOP)
                    self.val = None
                elif (len(self.t)>=51):
                    self.transitionTo(self.S2_STOP)    
                else:
                    self.transitionTo(self.S1_COLLECT)
                
            elif (self.state == self.S2_STOP):
                for n in range(len(self.t)):
                    self.myuart.write('{:},{:}\r\n'.format(self.t[n], self.x[n]))    
                self.transitionTo(self.S0_INIT)
             
            self.runs += 1   
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
## Defines the Encoder class object       
Enco = EncoderDriver(pyb.Timer(3), pyb.Pin.cpu.A6, pyb.Pin.cpu.A7)

## Defines the Data Collect task object
task = Data_Collect(Enco)

while True:
    
        task.run() # Runs the task indefinitely

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        