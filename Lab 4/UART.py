# -*- coding: utf-8 -*-
'''
@UART.py 
@brief A file which is the front end user interface for data collection
and graphing.
'''
import numpy as np
from matplotlib import pyplot
import time
import serial
ser = serial.Serial(port='COM3',baudrate=115200,timeout=1)

## time list
t = []
## position list
p = []

def Collect():
    '''
    @brief A function which prompts the user for an input and initiates data 
    collection
    '''
    inv = input('Press g to start data collection:')
    #inp = input('Press s to stop at any time')
    ser.write(str(inv).encode('ascii'))
    #ser.write(str(inp).encode('ascii'))
    inv = input('Press s to stop data collection before 10 seconds')
    ser.write(str(inv).encode('ascii'))
    n=0
    time.sleep(1)
   
    if ser.in_waiting: 
        while n < 51:
            myval = ser.readline().decode('ascii')
            val_list = myval.strip().split()
            print(myval)
            t.append(val_list[0])
            p.append(val_list[1])
            n+=1
Collect()
ser.close()


motor_position = np.array([t, p])
np.savetxt(motor_position.csv, motor_position, delimiter = ',')
pyplot.plot(t, p)
pyplot.xlabel('Time [ms]')
pyplot.ylabel('Motor Position [degrees]')



