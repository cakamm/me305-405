
'''
@file lightblink.py
@brief A file which creates a finite-state-machine which blinks an imaginary LED.
The user specifies a time interval in seconds in which they want an LED to blink on
and off.
'''

import time

class TaskBlink:
    '''@brief a finite state machine that controls a blinking LED
       @details This class implements a finite state machine to control blinking
       of a "virtual" LED light'''
       
    ## Constant defining State 0 - Initialization
    S0_INIT                  = 0
    
    ## Constant defining State 1
    S1_LED_OFF               = 1
    
    ## Constant defining State 2
    S2_LED_ON                = 2
    
    def __init__(self, period, LED):
        '''
        @brief creates a TaskBlink object

        Parameters
        ----------
        LED : condition of imaginary LED light.

        Returns
        -------
        None.

        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the LED object
        self.LED = LED
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
        
        ## The interval of time, in seconds, between runs of the task
        self.period = period
        
         ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.period
        
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print( '{:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_LED_OFF)
                self.LED.off()
                
            elif (self.state == self.S1_LED_OFF):
                print( '{:0.2f}'.format(self.curr_time - self.start_time))
                self.transitionTo(self.S2_LED_ON)
                self.LED.on()
                    
            elif (self.state == self.S2_LED_ON):
                print( '{:0.2f}'.format(self.curr_time - self.start_time))
                self.transitionTo(self.S1_LED_OFF)
                self.LED.off()
                    
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
                
            self.next_time += self.period # updating the "Scheduled" timestamp
            
    def transitionTo(self, newState):
        '''
        @brief  Updates the variable defining the next state to run
        '''
        self.state = newState
            
class LED:
    '''
    @brief      An LED.
    @details    This class represents an LED that can turn on and off.
    '''
    
    def __init__(self):
        '''
        @brief Creates an LED Object
        '''
        pass
    
    def on(self):
        '''
        @brief Turns LED on
        '''
        print('LED ON')
    
    def off(self):
        '''
        @brief Turns LED off
        '''
        print('LED OFF')