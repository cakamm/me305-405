'''
@file mainpattern.py
@brief A file which runs two LED tasks at once.

This file runs two finite-state-machine implementations at once. Task 1 blinks a
virtual LED on and off at a specified time interval. Task 2 alters the brightness
of a physical LED in a sawtooth pattern with a period of ten seconds. The period
may be changed by changing the time inteval (in seconds), which is representative
of the time increase per one hundredth increase in LED brightness.
'''

from LEDpattern import TaskPattern
from lightblink import LED, TaskBlink

## LED Object
LED = LED()

## Task 1 object
task1 = TaskBlink(5,LED)
## Task 2 object
task2 = TaskPattern(.1)

while True:
    task1.run()
    task2.run()