'''
@file LEDpattern.py
@brief A file which implements a finite-state-machine to control an LED's brightness in a sawtooth pattern

'''

import utime
import pyb
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)



class TaskPattern:
    '''@brief a finite state machine that changes the brightness of an LED in a sawtooth pattern
       @details This class implements a finite state machine to control the changing
       brightness of an LED light in a sawtooth pattern'''
       
    ## Constant defining State 0 - Initialization
    S0_INIT                  = 0
    
    ## Constant defining State 1
    S1_LED_RISING            = 1
    
    ## Constant defining State 1
    S2_LED_RESTART           = 2
    
    def __init__(self, interval):
        '''
        @brief creates a TaskPattern object

        Parameters
        ----------
        

        Returns
        -------
        None.

        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The number of seconds since Jan 1. 1970
        
        ## The interval of time, in seconds, between runs of the task
        self.interval = int(interval*1e6)
        
         ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time,self.interval)
        
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time,self.next_time) >= 0):
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                global n
                n = 0
                self.transitionTo(self.S1_LED_RISING)
                t2ch1.pulse_width_percent(0)
                
            elif (self.state == self.S1_LED_RISING):
                if n > 100:
                    self.transitionTo(self.S2_LED_RESTART)
                else:    
                    t2ch1.pulse_width_percent(n)
                    n = n+1
                    self.transitionTo(self.S1_LED_RISING)
            elif (self.state == self.S2_LED_RESTART):  
                n = 0
                self.transitionTo(self.S1_LED_RISING)        
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time = utime.ticks_add(self.next_time, self.interval) # updating the "Scheduled" timestamp
            
    def transitionTo(self, newState):
        '''
        @brief  Updates the variable defining the next state to run
        '''
        self.state = newState
            
